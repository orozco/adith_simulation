#!/bin/bash

# Este script corre el programa laberinto
#
# Author: Efrain Orozco <efrainorozco@linuxmail.org>

# Version de python valida
PYTHON_VERSION=2.7

if which python >/dev/null
then
  if ! (test "$(echo `python2 -c 'import sys;print(sys.version_info[:2])'`)" = "(2, 7)")
  then
    echo "Parece que python v${PYTHON_VERSION} no esta instalado en el sistema"
  fi
else
  echo "Parece que python v${PYTHON_VERSION} no esta instalado en el sistema"
fi

BINPATH=`dirname $0`
python2 "$BINPATH/../laberinto/main.py" $@
