from collections import deque
from heapq import heappush, heappop, heapify

from busqueda_excepcion import BusquedaExcepcion


class Busqueda:
    
    def nombre_busqueda(self):
        
        raise NotImplementedError
    
    def es_meta(self):

        return self._posicion_jugador == self._meta

    def hay_solucion(self):

        return len(self._abiertos) != 0

    def es_sucesor(self, candidato):

        raise NotImplementedError
    
    def encontrar(self):

        while self.hay_solucion():
            if self.es_meta():
                break
            # Si no es meta buscar otro candidato
            self.proxima_posicion()
    
    def proxima_posicion(self):

        raise NotImplementedError

    def reconstruir_camino(self):

        lista = list()
        elemento = None
        #Se busca primero la posicion final (que no necesariamente es la ultima del arreglo).
        for k in xrange(len(self._camino_final)):
            if self._camino_final[k][0] == self._meta:
                elemento = self._camino_final[k]
                break

        #Si es que no se encontro solucion no hay camino final.
        if elemento is None:
            return lista

        #Se busca los padres mientras no sea el inicio.
        while elemento[1] != self._posicion_inicial:
            for i in xrange(0, len(self._camino_final)):
                if self._camino_final[i][0] == elemento[1]:
                    elemento = self._camino_final[i]
                    lista.append(elemento[0])
                    break
        return lista


class BusquedaEnAnchura(Busqueda):


    def __init__(self, laberinto, opciones):

        self._laberinto = laberinto
        self._posicion_jugador = self._laberinto.obtener_posicion_inicial_jugador()
        self._posicion_inicial = self._posicion_jugador
        self._abiertos = deque([self._posicion_jugador])
        self._meta = self._laberinto.obtener_posicion_meta()
        self._opciones = opciones
        self._camino_final = [(self._posicion_inicial, None)]
    
    def nombre_busqueda(self):
        return "Busqueda En Anchura"
    
    def es_sucesor(self, candidato):
        lista = list(self._abiertos)
        for i in xrange(len(lista)):
            if lista[i] == candidato:
                return True
        return False

    def proxima_posicion(self):
        mapa = self._laberinto.obtener_matriz_laberinto()
        # Se libera la posicion actual. "4" significa "ya visitado"
        mapa[self._posicion_jugador[0]][self._posicion_jugador[1]] = 4
        self._posicion_jugador = self._abiertos.popleft()
        # Nueva posicion del jugador. "2" significa "jugador"
        mapa[self._posicion_jugador[0]][self._posicion_jugador[1]] = 2
        sucesores = self._laberinto.obtener_posiciones_libres(self._posicion_jugador)
        for sucesor in sucesores:
            if not self.es_sucesor(sucesor):
                self._abiertos.append(sucesor)
                self._camino_final.append((sucesor, self._posicion_jugador))


class BusquedaEnProfundidad(Busqueda):


    def __init__(self, laberinto, opciones):

        self._laberinto = laberinto
        self._posicion_jugador = self._laberinto.obtener_posicion_inicial_jugador()
        self._posicion_inicial = self._posicion_jugador
        self._abiertos = deque([self._posicion_jugador])
        self._meta = self._laberinto.obtener_posicion_meta()
        self._opciones = opciones
        self._camino_final = [(self._posicion_inicial, None)]
    
    def nombre_busqueda(self):
        return "Busqueda En Profundidad"
    
    def es_sucesor(self, candidato):
        lista = list(self._abiertos)
        for i in xrange(len(lista)):
            if lista[i] == candidato:
                return True
        return False

    def proxima_posicion(self):
        mapa = self._laberinto.obtener_matriz_laberinto()
        # Se libera la posicion actual. "4" significa "ya visitado"
        mapa[self._posicion_jugador[0]][self._posicion_jugador[1]] = 4
        self._posicion_jugador = self._abiertos.pop()
        # Nueva posicion del jugador. "2" significa "jugador"
        mapa[self._posicion_jugador[0]][self._posicion_jugador[1]] = 2
        sucesores = self._laberinto.obtener_posiciones_libres(self._posicion_jugador)
        for sucesor in sucesores:
            if not self.es_sucesor(sucesor):
                self._abiertos.append(sucesor)
                self._camino_final.append((sucesor, self._posicion_jugador))


class BusquedaCostoUniforme(Busqueda):


    def __init__(self, laberinto, opciones):

        self._laberinto = laberinto
        self._posicion_jugador = self._laberinto.obtener_posicion_inicial_jugador()
        self._posicion_inicial = self._posicion_jugador
        self._costo_actual = 0
        self._abiertos = [(self._costo_actual, self._posicion_jugador)]
        heapify(self._abiertos)
        self._meta = self._laberinto.obtener_posicion_meta()
        self._opciones = opciones
        self._camino_final = [(self._posicion_inicial, None)]
    
    def nombre_busqueda(self):
        return "Busqueda Costo Uniforme"
    
    def proxima_posicion(self):
        mapa = self._laberinto.obtener_matriz_laberinto()
        # Se libera la posicion actual. "4" significa "ya visitado"
        mapa[self._posicion_jugador[0]][self._posicion_jugador[1]] = 4
        nodo_actual = heappop(self._abiertos)
        self._costo_actual = nodo_actual[0]
        self._posicion_jugador = nodo_actual[1]
        # Nueva posicion del jugador. "2" significa "jugador"
        mapa[self._posicion_jugador[0]][self._posicion_jugador[1]] = 2
        sucesores = self._laberinto.obtener_posiciones_libres(self._posicion_jugador)
        lista = list(self._abiertos)
        for sucesor in sucesores:
            costo = self._costo_actual + 1
            # Flag indica si es que existe o no ya en el heap el par (x,y).
            flag = False
            for i in xrange(len(lista)):
                if lista[i][1] == sucesor:
                    flag = True
                    if lista[i][0] >= costo:
                        # Debe cambiar el costo.
                        lista[i][0] = costo
                        # Debo heapifiar.
                        heapify(lista)
                        # Esto es para reconstruir el camino.
                        for j in xrange(0, len(self._camino_final)):
                            if self._camino_final[j][0] == sucesor:
                                self._camino_final[j] = (sucesor, self._posicion_jugador)
            # Finalmente, los que no existiesen se agregan.
            if not flag:
                heappush(lista, [costo, sucesor])
                # Esto es para reconstruir el camino.
                self._camino_final.append((sucesor, self._posicion_jugador))
        #Se actualiza el heap.
        self._abiertos = lista


class BusquedaAEstrella(Busqueda):
    

    def __init__(self, laberinto, opciones):

        self._laberinto = laberinto
        self._posicion_jugador = self._laberinto.obtener_posicion_inicial_jugador()
        self._posicion_inicial = self._posicion_jugador
        self._meta = self._laberinto.obtener_posicion_meta()
        self._costo_actual = self._funcion_heuristica(self._posicion_inicial)
        self._abiertos = [[self._costo_actual, self._posicion_jugador]]
        heapify(self._abiertos)
        self._opciones = opciones
        self._camino_final = [(self._posicion_inicial, None)]
    
    def nombre_busqueda(self):
        return "Busqueda A*"
    
    def _funcion_heuristica(self, sucesor):
        
        dx = abs(sucesor[0] - self._meta[0])
        dy = abs(sucesor[1] - self._meta[1])
        # Distancia de Manhattan!
        heuristica = dx + dy
        # Se agrega un factor rompe empates.
        p = 0.001
        return heuristica * (1 + p)

    def proxima_posicion(self):
        mapa = self._laberinto.obtener_matriz_laberinto()
        # Se libera la posicion actual. "4" significa "ya visitado"
        mapa[self._posicion_jugador[0]][self._posicion_jugador[1]] = 4
        nodo_actual = heappop(self._abiertos)
        self._posicion_jugador = nodo_actual[1]
        self._costo_actual = nodo_actual[0] - self._funcion_heuristica(self._posicion_jugador)
        # Nueva posicion del jugador. "2" significa "jugador
        mapa[self._posicion_jugador[0]][self._posicion_jugador[1]] = 2
        sucesores = self._laberinto.obtener_posiciones_libres(self._posicion_jugador)
        lista = list(self._abiertos)
        for sucesor in sucesores:
            heuristica = self._funcion_heuristica(sucesor)
            # Flag indica si es que existe o no ya en el heap el par (x,y).
            flag = False
            for i in xrange(len(lista)):
                if lista[i][1] == sucesor:
                    flag = True
                    if lista[i][0] >= self._costo_actual + 1 + heuristica:
                        # Debe cambiar el costo.
                        lista[i][0] = self._costo_actual + 1 + heuristica
                        # Debo heapifiar.
                        heapify(lista)
                        # Esto es para reconstruir el camino.
                        for j in xrange(0, len(self._camino_final)):
                            if self._camino_final[j][0] == sucesor:
                                self._camino_final[j] = (sucesor, self._posicion_jugador)
            # Finalmente, los que no existiesen se agregan.
            if not flag:
                heappush(lista, [self._costo_actual + 1 + heuristica, sucesor])
                # Esto es para reconstruir el camino.
                self._camino_final.append((sucesor, self._posicion_jugador))

        #Se actualiza el heap.
        self._abiertos = lista
