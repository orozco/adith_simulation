import os

from laberinto_excepcion import LaberintoExcepcion


class Laberinto:
    
    
    def __init__(self, opciones):
        
        self._opciones = opciones

        # Si no hay un mapa como entrada se usa el mapa de ejemplo
        if self._opciones.mapa:
            self._mapa_path = self._opciones.mapa
        else:
            self._mapa_path = self._input_path()

        self._mapa = self._leer_mapa()
        self._filas = len(self._mapa)
        self._columnas = len(self._mapa[0])

        # 3 representa a la meta
        self._posicion_meta = self.obtener_posicion(3)

        # 2 representa al jugador
        self._posicion_inicial_jugador = self.obtener_posicion(2)

    @staticmethod
    def _input_path():

        pathfile = os.path.dirname(os.path.abspath(__file__))
        return os.path.join(pathfile, "..", "laberintos_txt", "laberinto.txt")

    def _leer_mapa(self):
        
        # Primero verifica si el archivo existe, sino existe crea una excepcion
        if os.path.isfile(self._mapa_path) == False:
            error_msg = "Archivo de entrada no existe: " + self._mapa_path
            raise LaberintoExcepcion(error_msg)

        # Lee el archivo de entrada y a la vez remueve el salto de linea al final
        lineas = [linea.strip() for linea in open(self._mapa_path)]

        # Transforma un caracter en una representacion numerica. Donde:
        # # => Define una pared del laberinto
        # . => Define un camino libre
        # D => Define a Dedalo (el inicio y posterior salida)
        # M => Define al Queso
        mapa = []
        for i in xrange(len(lineas)):
            lista = []
            cadena = lineas[i]

            for j in xrange(len(cadena)):
                if cadena[j] == "#":
                    lista.append(0)
                elif cadena[j] == ".":
                    lista.append(1)
                elif cadena[j] == "D":
                    lista.append(2)
                elif cadena[j] == "M":
                    lista.append(3)

            mapa.append(lista)

        return mapa

    def obtener_posicion(self, objeto):

        for f in xrange(self._filas):
            for c in xrange(self._columnas):
                if self._mapa[f][c] == objeto:
                    return [f, c]

    def obtener_filas(self):

        return self._filas

    def obtener_columnas(self):

        return self._columnas

    def obtener_matriz_laberinto(self):

        return self._mapa

    def obtener_posicion_meta(self):

        return self._posicion_meta

    def obtener_posicion_inicial_jugador(self):

        return self._posicion_inicial_jugador

    def obtener_posiciones_libres(self, posicion_objeto):

        libres = []
        deltaf = [-1, 0, 1, 0]
        deltac = [0, -1, 0, 1]
        for i in xrange(4):
            nuevof = posicion_objeto[0] + deltaf[i]
            nuevoc = posicion_objeto[1] + deltac[i]
            # Se verifica que la nueva coordenada este dentro de la matriz.
            if 0 <= nuevof < self._filas and 0 <= nuevoc < self._columnas:
                # Se verifica si es que no es una muralla ni ha sido recorrido antes.
                if self._mapa[nuevof][nuevoc] == 1 or self._mapa[nuevof][nuevoc] == 3:
                    libres.append([nuevof, nuevoc])

        return libres

    def __str__(self):

        mapa_str = ""

        for f in xrange(self._filas):
            for c in xrange(self._columnas):
                if self._mapa[f][c] == 0:
                    mapa_str += "# "
                elif self._mapa[f][c] == 1:
                    mapa_str += "  "
                elif self._mapa[f][c] == 2:
                    mapa_str += "D "
                elif self._mapa[f][c] == 3:
                    mapa_str += "M "
                elif self._mapa[f][c] == 4:
                    mapa_str += "* "
                elif self._mapa[f][c] == 5:
                    mapa_str += "  "
            mapa_str += '\n'

        return mapa_str
