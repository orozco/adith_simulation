
import traceback
import sys
import datetime

from procesos import *


def check_version():

    if sys.version_info[:2] != (2, 7):
        raise Exception("Parece que python v2.7 no esta instalado en el sistema")


if __name__ == '__main__':
    try:
        # Verificar version de python
        check_version()
        
        opciones = Opciones()
        opts = opciones.parse(sys.argv[1:])
        
        laberinto = Laberinto(opts)
        busqueda = None

        if opts.bea:
            print "Ejecutando Busqueda en Anchura..."
            busqueda = BusquedaEnAnchura(laberinto, opts)
        elif opts.bep:
            print "Ejecutando Busqueda en Profundidad..."
            busqueda = BusquedaEnProfundidad(laberinto, opts)
        elif opts.bcu:
            print "Ejecutando Busqueda Costo Uniforme..."
            busqueda = BusquedaCostoUniforme(laberinto, opts)
        elif opts.bae:
            print "Ejecutando Busqueda A*..."
            busqueda = BusquedaAEstrella(laberinto, opts)
        else:
            print "Ejecutando Busqueda en Anchura..."
            busqueda = BusquedaEnAnchura(laberinto, opts)
        
        if not opts.tiempo:
            despliegue = Despliegue(laberinto, busqueda, opts)
            despliegue.comenzar()
        else:
            start = datetime.datetime.now()
            busqueda.encontrar()
            #print busqueda.reconstruir_camino()
            print "La ejecucion tomo:", str(datetime.datetime.now() - start)
    except Exception, err:
        print traceback.format_exc()
    finally:
        sys.exit()
